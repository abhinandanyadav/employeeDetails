import { Pipe, PipeTransform } from '@angular/core';
import { Employee } from '../models/employee';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(employees: Employee[], args: any[]): any {
    
    if (args) {
      
      if (args != undefined || args != null) {
        return employees.filter(customer => customer.name.toLowerCase().indexOf(args[0].toLowerCase() ) !== -1 || customer.address.city.toLowerCase().indexOf(args[0].toLowerCase())!=-1);

      }
      else{
        return employees;
      }
    }
    else {
      
      return employees;
    }

  }

}
