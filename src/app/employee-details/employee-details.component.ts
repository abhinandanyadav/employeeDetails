import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { Employee } from '../models/employee';
import{Router} from '@angular/router';
import{SearchPipe} from '../pipe/search.pipe';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styles: []
})
export class EmployeeDetailsComponent implements OnInit {
employees = [];
  constructor( private employeeService:EmployeeService,private router:Router) { }

  ngOnInit() {
    this.employees=this.employeeService.getEmployess();
  }
  editEmployeeDetails(id){
   
     this.router.navigate(['/employees', id,'edit']);
  }

}
