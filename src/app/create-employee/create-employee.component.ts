import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Employee } from '../models/employee';
import { EmployeeService } from '../services/employee.service';


@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styles: []
})
export class CreateEmployeeComponent implements OnInit {
  employeeFromGroup: FormGroup;
  id: Number;
  employees = [];
  constructor(private router: Router, private activeRoute: ActivatedRoute, private employeeService: EmployeeService, private fb: FormBuilder) {

    this.employeeFromGroup = this.fb.group({
      'id': [null],
      'name': [null, [Validators.required, Validators.minLength(4)]],
      'phone': [null, [Validators.required,Validators.pattern('^[0-9]*$')]],      
      'address': this.fb.group({
        'city': [null],
        'address1': [null],
        'address2': [null],
        'postalCode': [null],
      })
    });
  }

  ngOnInit() {
    this.id = this.activeRoute.snapshot.params['id'];
    console.log(this.id);
    this.employees = this.employeeService.getEmployess();
    console.log(this.employees);
    if (this.id != undefined) {
      if (this.employees.length > 0) {
        this.employeeFromGroup.controls['id'].setValue(this.employees[0].id);
        this.employeeFromGroup.controls['name'].setValue(this.employees[0].name);
  
        this.employeeFromGroup.controls['phone'].setValue(this.employees[0].phone);
        this.employeeFromGroup.controls['address'].setValue(this.employees[0].address);
      }
      else {
        this.router.navigate(['employees']);
      }


    }
    else {
      this.employeeFromGroup.controls['id'].setValue(this.employees.length + 1);
    }

  }

  SaveData(form: NgForm) {
    console.log(form.valid);
    if (form.valid) {
      if (this.id != undefined) {
        if(this.employeeService.updateEmployee(form.value)){
           this.router.navigate(['employees']);
        }
      }
      else {
        if (this.employeeService.addEmployee(form.value)) {
          this.router.navigate(['employees']);
        }
      }
    }
  }
}
