import { Injectable } from '@angular/core';
import { Employee } from '../models/employee'


@Injectable()
export class EmployeeService {
 employees = [];
  constructor() { }

  addEmployee(employees:Employee):boolean{
    this.employees.push(employees);
    return true;
  }
  getEmployess():Employee[]{
    return this.employees;
  }
  updateEmployee(employee:Employee):boolean{
    var index=this.employees.findIndex(x => x.id==employee.id);
    this.employees[index]=employee;
   
    return true;
  }

}
