import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NameRoutingModule, routedComponents } from './route';
import { EmployeeService } from './services/employee.service';
import { SearchPipe } from './pipe/search.pipe';
import { PhoneValidateComponent } from './phone-check/phone-validate.component';


@NgModule({
  declarations: [
    AppComponent,
    routedComponents,
    SearchPipe,
    PhoneValidateComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NameRoutingModule,
    ReactiveFormsModule
  ],
  providers: [EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
