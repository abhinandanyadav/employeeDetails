export class Employee {
    id:number;
    name: string;
    phone: string;
    address: {
        city:string;
        address1:string;
        address2:string;
        postalCode:string
    };
    constructor() { }
}