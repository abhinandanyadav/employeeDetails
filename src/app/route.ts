import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { CreateEmployeeComponent } from './create-employee/create-employee.component'
import { PhoneCheckComponent } from './phone-check/phone-check.component';
import { SearchTask2Component } from './search-task2/search-task2.component';

const routes: Routes = [
    { path: 'employees', component: EmployeeDetailsComponent },
    { path: 'employees/add', component: CreateEmployeeComponent },
    { path: 'employees/:id/edit', component: CreateEmployeeComponent },
    { path: 'task2', component: SearchTask2Component },
     { path: 'task1', component: PhoneCheckComponent },
    { path: '', redirectTo: 'employees', pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class NameRoutingModule { }

export const routedComponents = [EmployeeDetailsComponent, CreateEmployeeComponent,PhoneCheckComponent,SearchTask2Component];